package org.formation.spring.dao;

import java.util.List;

import org.formation.spring.model.Client;
import org.formation.spring.model.Conseiller;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The implementation of  interface is not required It is generated
 * by Spring Data JPA Framework This interface is responsible for CRUD + Conseiller
 * queries based on query methods and parameter name and types
 */
public interface CrudConseillerDAO extends JpaRepository<Conseiller, Integer>{
	
	List<Conseiller> findConseillerByNom(String nom);
//	public Conseiller login(String login, String motDePasse);
	
	public Conseiller login(String login, String motDePasse);
	
	

}
