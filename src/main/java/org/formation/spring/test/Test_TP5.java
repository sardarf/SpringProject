package org.formation.spring.test;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.service.IPrestiBanqueService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test_TP5 {

	public static void main(String[] args) {
		ApplicationContext  context = new AnnotationConfigApplicationContext(ApplicationConfig.class);		   


		IPrestiBanqueService service = context.getBean("service", IPrestiBanqueService.class);
//		service.listClients();
		
//		service.addClient(new Client("Tommy", "Hilfiger", "day", "night", new Adresse(20, "Rue de l'Est", "Paris")));
		
//		Client c=service.editClient(7);
////		c.setId(25);
//		c.setNom("Nico");
//		c.setPrenom("Muriel");
//		c.setLogin("hap");
//		c.setMotDePasse("end");
//		c.setAdresse(new Adresse (150, "Rue de Rome", "Paris"));
//		service.updateClient(c);
		
		
//		service.listClients();
//		System.out.println(service.listClients());
		


	    ((ConfigurableApplicationContext)(context)).close();

	}

}
