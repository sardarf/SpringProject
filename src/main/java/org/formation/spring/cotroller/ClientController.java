package org.formation.spring.cotroller;

import java.util.List;

import javax.persistence.Embedded;

import org.formation.spring.dao.CrudClientDAO;
import org.formation.spring.dao.CrudConseillerDAO;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.model.Conseiller;
import org.formation.spring.model.Employe;
import org.formation.spring.service.IPrestiBanqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClientController {
	
	
//	@Autowired
//	private CrudClientDAO crudClientDAO;
//	
//	@RequestMapping(value="/displayClients", method=RequestMethod.GET)
//	
//	public ModelAndView ListClients() {
//		List<Client> clients=crudClientDAO.findAll();
//		return new ModelAndView("displayClients", "clients", clients);
//		
//	}
	
	
	@Autowired
	
	private IPrestiBanqueService prestiBanqueService;
	
	@RequestMapping(value="/Ecran3", method=RequestMethod.GET)
	public ModelAndView listClients() {
		List<Client> clients=prestiBanqueService.list();
		return new ModelAndView("Ecran3", "clients", clients);
		
	}
	
	
	@RequestMapping (value="/adCl", method=RequestMethod.GET)
	public String add(ModelMap modelMap) {
		modelMap.put("clients", new Client());
		return "adCl";
	}
	@RequestMapping (value="/ajoutClient", method=RequestMethod.GET)
	public String ajoutClient(ModelMap modelMap) {
		System.out.println("Je suis arrivé!");
		return "adCl";
	}
	
	
	@RequestMapping (value="/deleteClients/{id}", method=RequestMethod.GET)
	public String delete(@PathVariable("id") int idClient) {
		prestiBanqueService.deleteClient(idClient);
		return "deleteClients";
	}
	
	
	@RequestMapping (value="/editClients/{idClient}", method=RequestMethod.GET)
	public String edit(
			@PathVariable("idClient") int idClient, 
			ModelMap modelMap) {
		modelMap.put("displayClients", prestiBanqueService.editClient(idClient));
		return "redirect:/editClients";
	}
	
	
	@RequestMapping (value="/editClients", method=RequestMethod.POST)
	public String edit(@ModelAttribute(value="clients") Client clients,
			ModelMap modelMap) {
		prestiBanqueService.updateClient(clients);
		return "redirect:/displayClients";
	}
	
//	@RequestMapping(value="/displayClients", method=RequestMethod.GET)
//	public ModelAndView listClients() {
//		List<Client> clients=prestiBanqueService.listClients();
//		return new ModelAndView("displayClients", "clients", clients);
//		
//	}
	
//@Autowired
	
//	private IPrestiBanqueService prestiBanqueService;
	
	
	
	//Method Add Clients
	
//		@RequestMapping (value="/addClients", method=RequestMethod.GET)
//		public String add(ModelMap modelMap) {
//			modelMap.put("clients", new Client());
//			return "addClients";
//		}
		
		
		
		
		
//		@RequestMapping (value="/ajoutClient", method=RequestMethod.POST)
//		public String add(@RequestParam("nom") String nom,@RequestParam("prenom") String prenom, @RequestParam("telephone") String numtel, @RequestParam("numero") int numero,
//				 @RequestParam("rue") String rue, @RequestParam("ville") String ville,
//				ModelMap modelMap) {
//			Client c1 = new Client(id, nom,prenom,numtel, new Adresse(numero,rue,ville));
//			prestiBanqueService.add(c1);
//			private int id;
//			private String nom;
//			private String prenom;
//			private String numtel;
//			private int numero;
//			private String rue;
//			private String ville;
//			
//			@Autowired
//			@Embedded
//			private Adresse adresse;
			
			
//			//prestiBanqueService.updateClient(clients);
//			System.out.println("Nom Client "+nom);
//			return "redirect:/Ecran3";
//		}
//		
		//Method Delete
		
		
		
		
		
		
		//Method EditClients
		
		
		
		
		
		
		
		
		
//		@Autowired
//		private CrudConseillerDAO crudConseillerDAO;
////		
//		@RequestMapping(value="login" method=RequestMethod.GET)
//		public String login(ModelMap modelMap) {
//			modelMap.put("clients", CrudConseillerDAO.findall)
//		}
//		
//		@RequestMapping(value = "/loginEmploye", method = RequestMethod.POST)
//		public String recoverPass(@RequestParam("login") String login, @RequestParam("mdp") String mdp, ModelMap model) {
//			Employe employe = crudConseillerDAO.login(login, mdp);
//			if (employe != null) {
//				if (Conseiller.class.isAssignableFrom(employe.getClass())) {
//					LOGGER.debug("Type Conseiller: " + employe.getNom()()+" "+employe.getPrenom() + " ");				
//					model.addAttribute("ObjEmploye",employe);
//					model.addAttribute("tableauClient",crudClientDAO.findAllClientByConseiller(employe));				
//					return "accueilConseiller";
//				} else {
//					LOGGER.debug("Type Gérant: " + employe.getNomEmploye()+" "+employe.getPrenomEmploye() + " ");
//					return "displayClients";
//				}
//			} else {
//				 model.addAttribute("TestMessage",true);
//				 model.addAttribute("TextMessage","Incorrect username or password.");
//				 return "login";
//			}
//		}
	
	
	


}
