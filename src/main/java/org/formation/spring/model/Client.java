 package org.formation.spring.model;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * The Class Client.
 *
 * @author HLFM
 * The Class Client.
 * cette classe permet d'instancier des clients particulier et professionnel
 * avec tous ses attributs et les comptes et cartes bancaires
 */
@Component
@Entity
public class Client {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String nom;
	private String prenom;
	private String numtel;
	
	
	@Autowired
	@Embedded
	private Adresse adresse;
	
	@ManyToOne(cascade= {CascadeType.PERSIST})
	@JoinColumn(name="conseiller_id")
	private Conseiller conseiller;
	

	public Conseiller getConseiller() {
		return conseiller;
	}

	public void setConseiller(Conseiller conseiller) {
		this.conseiller = conseiller;
	}

	public Client() {}

	public Client(Adresse adresse) {
	    this.adresse = adresse;
	}
	
	
	

	public Client(int id, String nom, String prenom, String numtel, Adresse adresse) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.numtel = numtel;
		this.adresse = adresse;
	}

	
	
	public String toString() {
		return " ID  : " + this.getId() + " - " + " Nom : " + this.getNom() + " - " + " Prénom : " + this.getPrenom()
				+ " - " + " numtel : " + this.getNumtel() + " - " +

				" Adresse : " + this.getAdresse().toString();
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNumtel() {
		return numtel;
	}

	public void setNumtel(String numtel) {
		this.numtel = numtel;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}



	
	
	

  
}
