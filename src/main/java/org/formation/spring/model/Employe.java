package org.formation.spring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
/**
 * The Class Gerant.
 *
 * @author HLFM
 * The Class Employe.
 * la classe Employe est la classe mère de Class Conseiller
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Employe {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String login;
	private String motDePasse;

	private String nom = "";

	private String prenom = "";

	public Employe() {
	}

	public Employe(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
	}

	public Employe(int id, String nom, String prenom) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

}
