package org.formation.spring.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.criteria.Fetch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * The Class Conseiller.
 *
 * @author HLFM
 * The Class Conseiller.
 * la classe conseiller permet d'instancier un objet conseiller
 * il pourrra gerer un portefeuille de clients
 */
@Component
@Entity
public class Conseiller extends Employe {

	
	

	@OneToMany(mappedBy = "conseiller", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private List<Client> listeClients = new ArrayList<>();

	public Conseiller() {

	}

	public Conseiller(String nom, String prenom, String login, String motDePasse) {
		super(nom, prenom);
		

	}

	public String toString() {
		return " ID  : " + this.getId() + " - " + " Nom : " + this.getNom() + " - " + " Prénom : " + this.getPrenom()
				+ " - " ;

	}

	

}
