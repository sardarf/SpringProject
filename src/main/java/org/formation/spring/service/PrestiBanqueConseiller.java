package org.formation.spring.service;

import java.util.List;

import org.formation.spring.dao.CrudConseillerDAO;
import org.formation.spring.model.Client;
import org.formation.spring.model.Conseiller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**@author HLFM
 * The Class ProxibanqueConseillerServiceImplCrud.
 * cette classe service propose toutes les fonctionnalites du CRUD au conseiller
 */

@Service("service")
public class PrestiBanqueConseiller {

	@Autowired
	public CrudConseillerDAO crudConseillerDAO;
	
	
	public Conseiller loginAndPassword(String login, String motDePasse) {
		// TODO Auto-generated method stub
		return crudConseillerDAO.login(login, motDePasse);
	}

	
//	@Override
//	public List<Conseiller> listCons() {
//		// TODO Auto-generated method stub
//		return null;
//	}


}
