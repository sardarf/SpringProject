package org.formation.spring.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.formation.spring.dao.CrudClientDAO;
import org.formation.spring.dao.CrudConseillerDAO;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.model.Conseiller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**@author HLFM
 * The Class ProxibanqueGerantServiceImplCrud.
 * cette classe service propose toutes les fonctionnalites du CRUD au gerant
 */
@Service
public class PrestiBanqueServiceImplCrud implements IPrestiBanqueService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PrestiBanqueServiceImplCrud.class);

	@Autowired
	private CrudClientDAO crudClientDAO;
	
	

//	@Override
//	public void addClient(Client c) {
//		crudClientDAO.save(c);
//	}
//
//	@Override
//	public List<Client> listClients() {
//		
//		LOGGER.debug("lister clients");
//		LOGGER.info("information");
//		return crudClientDAO.findAll();
//		
//		
//	
//		
////		return listClients();
//
//	}
//
//	@Override
//	public void deleteClient(int idClient) {
//		crudClientDAO.delete(idClient);
//	}
//
//	@Override
//	public Client editClient(int idClient) {
//		return crudClientDAO.findOne(idClient);
//
//	}
//
//	@Override
//	public void updateClient(Client c) {
//		crudClientDAO.save(c);
//	}
//	
	

@Override
public void add(Client client) {
	crudClientDAO.save(client);
	
}

@Override
public List<Client> list() {
	// TODO Auto-generated method stub
	return crudClientDAO.findAll();
}

@Override
public void deleteClient(int id) {
	crudClientDAO.delete(id);
	
}

@Override
public Client editClient(int id) {
	// TODO Auto-generated method stub
	return crudClientDAO.findOne(id);
}

@Override
public void updateClient(Client client) {
	// TODO Auto-generated method stub
	crudClientDAO.save(client);
}

@PostConstruct
public void createSomeClient() {
	
	updateClient(new Client(1, "Tommy", "Hilfiger", "06952659", new Adresse(20, "Rue de l'Est", "Paris")));
}

//@Override
//public List<Conseiller> listCons() {
//	// TODO Auto-generated method stub
//	return null;
//}






}
